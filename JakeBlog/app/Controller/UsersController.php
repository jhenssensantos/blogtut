<?php 
App::uses('AppController', 'Controller');

class UsersController extends AppController{

	public function admin_index(){
		$this->set('users',$this->User->find('all'));
	}

	public function edit($id = null){
		if(!$id){
			throw new NotFoundException(__('Invalid Post'));
		}

		$user = $this->User->findById($id);
		if($this->request->is(array('post','put'))){
			$this->User->id = $id;
			if($this->User->save($this->request->data)){
					$this->Flash->success(__('Your post has been updated.'));
					return $this->redirect(array('action'=>'index'));
				}
				$this->Flash->error(__('Unable to update to your post'));
			}
			if(!$this->request->data){
				$this->request->data = $user;
			}
	}

	public function view($id = null){
		if(!$id){
			throw new NotFoundException(__('Invalid post'));
		}

		$post = $this->Post->findById($id);
		if(!$post){
			throw new NotFoundException(__('Invalid post'));
		}

		$this->set('post',$post);
	}

	public function delete($id){
		if($this->request->is('get')){
			throw new MethodNotAllowedException();
		}
		
		if ($this->User->delete($id)) {
       		$this->Flash->success(__('The post with id: %s has been deleted.', h($id)));
	    } else {
	        $this->Flash->error(__('The post with id: %s could not be deleted.', h($id)));
	    }
   		return $this->redirect(array('action' => 'index'));
	}


	public function register(){
		if($this->request->is('post')){
			if($this->User->save($this->request->data)){
				$this->Flash->success(__('Registered Successfully'));
				return $this->redirect(array('action' => 'login'));
			}
			$this->Flash->error(__('The registration failed. Please, try again.'));
		}
	}

	public function login(){
		if($this->request->is('post')){
			if($this->Auth->login()){
				// if($this->request->prefix == 'admin'){
				//     $this->Auth->loginAction = array('controller' => 'users', 'action' => 'admin_index', 'plugin' => false);
				// }
				return $this->redirect($this->Auth->redirectUrl());
			}
			$this->Flash->error(__('Invalid username or password, try again'));
		}
	}

	public function logout(){
		return $this->redirect($this->Auth->logout());
	}

	public function admin_logout(){
		return $this->redirect($this->Auth->logout());
	}
}
?>